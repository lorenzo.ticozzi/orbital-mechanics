function M = zEulerRotFr(angle)
    
    % Euler rotation around z axis

    M   = [ cos(angle), sin(angle), 0;
           -sin(angle), cos(angle), 0;
            0         , 0         , 1];

end

