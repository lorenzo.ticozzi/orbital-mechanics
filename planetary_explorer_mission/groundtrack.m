function [alpha,delta,latitude,longitude] = groundtrack(r,GST1,OmE,t)

    % alpha     = right ascension in ECEI Reference
    % delta     = declination in ECEI Reference
    % latitude  = with respect to geocentrical system
    % longitude = with respect to geocentrical system
    % r         = Radius of the orbit
    % GST1      = Grenwich Sideral Time at t = 0
    % w_planet  = planet rotation speed (rad/s)
    % t         = time

    % alpha & delta:
    proj_eq  = [r(:,1),r(:,2),zeros(size(r,1),1)];
    aries    = [1,0,0];
    alpha    = angle(proj_eq,aries);
    delta    = angle(r,proj_eq);
    alpha(r(:,2)<0) = 2*pi-alpha(r(:,2)<0);
    delta(r(:,3)<0) = -delta(r(:,3)<0);

    % latitude & longitude
    latitude  = delta*180/pi;
    longitude = (unwrap(alpha)-(GST1+OmE*t))*180/pi;
    longitude = longitude - 360*fix(longitude/360); 
end

function A = angle(x,y)
    A = 2*atan(matnorm(x.*matnorm(y)-matnorm(x).*y)./...
        matnorm(x.*matnorm(y)+matnorm(x).*y));
end

function Nx = matnorm(x)
    Nx = sqrt(x(:,1).^2+x(:,2).^2+x(:,3).^2);
end
