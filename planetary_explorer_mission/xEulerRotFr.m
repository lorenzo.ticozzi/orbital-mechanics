function M = xEulerRotFr(angle)
    
    % Euler rotation of frame around x axis

    M   = [ 1,           0,          0;
            0,  cos(angle), sin(angle);
            0, -sin(angle), cos(angle)];

end

