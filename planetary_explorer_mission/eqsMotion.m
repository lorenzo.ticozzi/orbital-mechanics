% eqsMotion.m: Cartesian equations of motion
%
% PROTOTYPE:
%   [t,r,v]   = eqsMotion(r0,v0,parameters,options,tspan)
%
% INPUT :
%   r0[3]       Vector containing initial position in Cartesian coordinates
%   v0[3]       Vector containing initial velocity in Cartesian coordinates
%   parameters  Vector containing the following parameters:
%       muE = parameters(1) gravitational parameter of Earth [km^3/s^2]
%       RE  = parameters(2); Earth radius [km]
%       J2  = parameters(3); J2 coefficient [-]
%       OmE = parameters(4); Earth's rotation rate [rad/s]
%       GST0= parameters(5); Initial Greenwich Sidereal Time []
%       muM = parameters(6); Moon's gravitational parameter [km^3/s^2]
%       jd0 = parameters(7); Initial julian date
%   options     ODE options variable
%   tspan       Vector of times in which the solution needs to be computed.
%
% OUTPUT:
%   t           Vector of times where the solution is known
%   r           Matrix of positions
%   v           Matrix of velocities
%
% FUNCTIONS CALLED:
%   OE2statevec,uplanet,xEulerRotFr
%
% AUTHORS:
%   - Michele Maranesi
%   - Lorenzo Ticozzi
%   - Salvador Ribes Izquierdo
%
% PREVIOUS VERSION: 
%
% CHANGELOG:
%   
% -------------------------------------------------------------------------

function [t,r,v] = eqsMotion(r0,v0,parameters, options,tspan)

    [t,y] = ode113(@(t,y)equations(t,y,parameters),...
        tspan,[r0(1),r0(2),r0(3),v0(1),v0(2),v0(3)],options);

    % Position and velocity
    r = y(:,1:3);
    v = y(:,4:6);
    
end

function dX = equations(t,y,parameters)

    % Extraction of orbital elements:
    rx = y(1);
    ry = y(2);
    rz = y(3);
    vx = y(4);
    vy = y(5);
    vz = y(6);

    % Parameters
    mu = parameters(1);

    % Perturbations
    ap = perturbations(rx,ry,rz,t,parameters);

    % Equations Of Motion in GeoEqFr IJK
    r       = sqrt(rx.^2+ry.^2+rz.^2);
    dX(1,1) = vx;
    dX(2,1) = vy;
    dX(3,1) = vz;
    dX(4,1) = -mu*rx/r.^3 + ap(1);
    dX(5,1) = -mu*ry/r.^3 + ap(2);
    dX(6,1) = -mu*rz/r.^3 + ap(3);
    
end

function [ap] = perturbations(rx,ry,rz,t,parameters)

    % Parameters
    muE  = parameters(1);
    RE	 = parameters(2);
    J2	 = parameters(3);
    OmE  = parameters(4);
    GST0 = parameters(5);
    Am   = parameters(6);
    jd0	 = parameters(7);
    CR   = parameters(8);

    % J2 perturbation in GeoEqFr IJK
    r	    = sqrt(rx.^2+ry.^2+rz.^2);
    a_J2(1) = muE/r^3*rx*(J2*3/2*(RE/r)^2*(5*(rz/r)^2-1));
    a_J2(2) = muE/r^3*ry*(J2*3/2*(RE/r)^2*(5*(rz/r)^2-1));
    a_J2(3) = muE/r^3*rz*(J2*3/2*(RE/r)^2*(5*(rz/r)^2-3));
    
    % Earth position in HelioEclipFr XYZ
    [kepE,~]= uplanet(jd0+t/(24*3600),3);   % Earth heliocentric OE
    [rE,~]	= OE2statevec(kepE(2),kepE(1),kepE(3),...
        kepE(4),kepE(5),kepE(6),muE);       % Earth pos in HelioEclipFr XYZ

    % Unit vector pointing from Sun to s/c in GeoEqFr IJK
    epsilon = (23+45/60)*pi/180;            % Earth eq/ecl tilt [rad]
    rE = xEulerRotFr(-epsilon)*rE';         % Earth position in GeoEqFr IJK
    r_sc_S  = [rE(1)+rx,rE(2)+ry,rE(3)+rz]; 
    u_sc_S  = r_sc_S/norm(r_sc_S);

    % Solar Radiation Pressure perturbation in GeoEqFr
    rS    = [-rE(1),-rE(2),-rE(3)];         % Sun position GeoEqFr IJK
    rSC   = [rx,ry,rz];                     % s/c position GeoEqFr IJK
    thP   = acos(dot(rS,rSC)/(norm(rS)*norm(rSC)));
    th1   = acos(6378/norm(rSC));
    th2   = acos(6378/norm(rS));
    if th1+th2 <= thP
        nu = 0;
    else
        nu = 1;
    end
    PSR     = 4.56e-6;          % Solar radiation pressure [N/m2]
    pSR     = nu*PSR*CR*Am/1000;% Solar radiation pert magnitude [kg/s2]
    a_PSR   = pSR*u_sc_S;       % Solar radiation pert accel GeoEqFr IJK

    % Sum of each contribution 
    ap      = a_J2 + a_PSR;     % perturbation acc GeoEqFr IJK
    
end