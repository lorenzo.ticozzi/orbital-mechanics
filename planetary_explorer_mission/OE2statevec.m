% Obtain r,v in the geocentric equatorial frame from orbital elements.
function [r,v] = OE2statevec(e,a,i,RAAN,w,theta,mu)

    % Preprocessing
    if length(e) < length(theta)
        e      = e*ones(length(theta),1);
        i      = i*ones(length(theta),1);
        a      = a*ones(length(theta),1);
        w      = w*ones(length(theta),1);
        RAAN   = RAAN*ones(length(theta),1);
    end

    % Derived parameters
    for j = 1:length(theta)
        p      = a(j)*(1-e(j)^2);
        R_pqw2IJK = ...
            [cos(RAAN(j))*cos(w(j))-sin(RAAN(j))*sin(w(j)).*cos(i(j)),...
            sin(RAAN(j))*cos(w(j))+cos(RAAN(j))*sin(w(j))*cos(i(j)),...
            sin(w(j))*sin(i(j));...
            -cos(RAAN(j))*sin(w(j))-sin(RAAN(j))*cos(w(j))*cos(i(j)),...
            -sin(RAAN(j))*sin(w(j))+cos(RAAN(j))*cos(w(j))*cos(i(j)),...
            cos(w(j))*sin(i(j));...
            sin(RAAN(j))*sin(i(j)),...
            -cos(RAAN(j))*sin(i(j)),...
            cos(i(j))];
    
    % State vectors (r,v) in Perifocal frame, pqw
    rp     = p/(1+e(j)*cos(theta(j)))*[cos(theta(j));sin(theta(j));0];
    vp     = sqrt(mu/p)*[-sin(theta(j));e(j)+cos(theta(j));0];
    
    % Conversion to Cartesian coordinate
    r(j,:) = R_pqw2IJK'*rp;
    v(j,:) = R_pqw2IJK'*vp;

end