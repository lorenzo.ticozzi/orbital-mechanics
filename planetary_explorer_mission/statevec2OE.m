function [e,a,i,RAAN,w,theta] = statevec2OE(r,v)
% Calculates the orbital elements associated to r,v in the geocentric 
% equatorial system. Radius and velocity come as nx3 matrices.

    % Parameters
    mu     = 398600.4; %Km^3/s^2

    % Derived parameters:
    h = cross(r,v);
    n = [-h(:,2)./sqrt(h(:,1).^2+h(:,2).^2),...
        h(:,1)./sqrt(h(:,1).^2+h(:,2).^2),...
        zeros(size(r,1),1)];
    Nh= sqrt(h(:,1).^2 + h(:,2).^2 + h(:,3).^2);
    Nr= sqrt(r(:,1).^2 + r(:,2).^2 + r(:,3).^2);
    Nv= sqrt(v(:,1).^2 + v(:,2).^2 + v(:,3).^2);
    E = 0.5*Nv.^2 - mu./Nr;

    % Eccentricity
    ev= cross(v,h)/mu-(r./Nr);
    e = sqrt(ev(:,1).^2+ev(:,2).^2+ev(:,3).^2);

    % Semimajor axis
    a = -mu./(2*E);

    % Anomaly
    theta = real(acos(dot(ev,r,2)./(e.*Nr)));
    theta(dot(r,v,2) < 0) = 2*pi - theta(dot(r,v,2) < 0); 

    % Inclination
    i = acos(h(:,3)./Nh);

    % RAAN
    RAAN = acos(n(:,1));
    RAAN(n(:,2) < 0) = 2*pi - RAAN(n(:,2) < 0);

    % Perigee angle
    w = acos(dot(ev,n,2)./e);
    w(ev(:,3) < 0) = 2*pi - w(ev(:,3) < 0);
end
