% Gauss orbit propagation equations
%
% PROTOTYPE:
%   [t,e,a,i,RAAN,w,theta] = ...
%       gaussequations(e0,a0,i0,RAAN0,w0,theta0,parameters,options,tspan)
%
% INPUT:
%   e0[1]       Initial eccentricity of the orbit []
%   a0[1]       Initial semimajor axis of the orbit [km]
%   i0[1]       Initial inclination of the orbit [rad]
%   RAAN0[1]    Initial RAAN of the orbit [rad]
%   w0[1]       Initial argument of perigee of the orbit [rad]
%   theta0[1]   Initial true anomaly of the orbit [rad]
%   parameters  Vector containing the following parameters:
%       muE = parameters(1) gravitational parameter of Earth [km^3/s^2]
%       RE  = parameters(2); Earth radius [km]
%       J2  = parameters(3); J2 coefficient [-]
%       OmE = parameters(4); Earth's rotation rate [rad/s]
%       GST0= parameters(5); Initial Greenwich Sidereal Time []
%       muM = parameters(6); Moon's gravitational parameter [km^3/s^2]
%       jd0 = parameters(7); Initial julian date
%   options     Ode options variable
%   tspan       Vector of times for computing solution [T]
%
% OUTPUT:
%   t           Vector of times where the solution is known
%   e           Vector of eccentricities
%   a           Vector of semimajor axis of the orbit [km]
%   i           Vector of inclinations of the orbit [rad]
%   RAAN        Vector of RAANs of the orbit [rad]
%   w           Vector of arguments of perigee of the orbit [rad]
%   theta       Vector of true anomaly of the orbit [rad]
%
% FUNCTIONS CALLED:
%   OE2statevec,uplanet,xEulerRotFr
%
% AUTHORS:  
%   - Michele Maranesi
%   - Lorenzo Ticozzi
%   - Salvador Ribes Izquierdo
%
% PREVIOUS VERSION:   
%
% CHANGELOG:  
%
% -------------------------------------------------------------------------
function [t,e,a,i,RAAN,w,theta] = ...
    eqsGauss(e0,a0,i0,RAAN0,w0,theta0,parameters,options,tspan)

    [t,y] = ode113(@(t,y)equations(t,y,parameters),tspan,...
        [a0,e0,i0,w0,theta0,RAAN0],options);

    % Extraction of Orbital Elements
    a	  = y(:,1);
    e	  = y(:,2);
    i     = y(:,3);
    w     = mod(y(:,4),2*pi);
    theta = mod(y(:,5),2*pi);
    RAAN  = mod(y(:,6),2*pi);
end

function yp = equations(t,y,parameters)

    % Orbital elements:
    a	  = y(1);
    e	  = y(2);
    i	  = y(3);
    w	  = y(4);
    theta = y(5);
    RAAN  = y(6);

    % Parameters
    mu    = parameters(1);

    % Derived parameters:
    b      = a*sqrt(1-e^2);
    p      = b^2/a;
    r      = p/(1+e*cos(theta));
    v      = sqrt(2*mu/r-mu/a);
    n      = sqrt(mu/a^3);
    h      = n*a*b;
    thetax = theta+w;

    % Perturbations
    [at,an,ah] = perturbations(e,a,i,RAAN,w,theta,t,parameters);

    % Gauss derivatives (non inertial tnh orbital frame)
    yp(1,1)= 2*v*a^2/mu*at;                                 % da/dt
    yp(2,1)= 1/v*(2*(e+cos(theta))*at-r/a*sin(theta)*an);   % de/dt
    yp(3,1)= r*cos(thetax)/h*ah;                            % di/dt
    yp(4,1)= 1/e/v*(2*sin(theta)*at+(2*e+r/a*cos(theta))*an)...
        -r*sin(thetax)*cos(i)/h/sin(i)*ah;                  % dw/dt
    yp(5,1)= h/r^2-1/(e*v)*...
        (2*sin(theta)*at+(2*e+r/a*cos(theta))*an);          % dtheta/dt
    yp(6,1)= r*sin(thetax)/h/sin(i)*ah;                     % dRAAN/dt
end

function [at,an,ah] = perturbations(e,a,i,RAAN,w,theta,t,parameters)

    % Parameters
    muE  = parameters(1);
    RE	 = parameters(2);
    J2	 = parameters(3);
    OmE  = parameters(4);
    GST0 = parameters(5);
    Am   = parameters(6);
    jd0	 = parameters(7);
    CR   = parameters(8);

    % Cartesian
    [r,v] = OE2statevec(e,a,i,RAAN,w,theta,muE);
    rx	  = r(1);
    ry    = r(2);
    rz    = r(3);

    % J2 in Earth Centered Cartesian Coordinates IJK
    r_mod	= norm(r);
    a_J2(1) = muE/r_mod^3*rx*(J2*3/2*(RE/r_mod)^2*(5*(rz/r_mod)^2-1));
    a_J2(2) = muE/r_mod^3*ry*(J2*3/2*(RE/r_mod)^2*(5*(rz/r_mod)^2-1));
    a_J2(3) = muE/r_mod^3*rz*(J2*3/2*(RE/r_mod)^2*(5*(rz/r_mod)^2-3));

    % Earth position
    [kepE,~]= uplanet(jd0+t/(24*3600),3);   % Earth heliocentric OE
    [rE,~]	= OE2statevec(kepE(2),kepE(1),kepE(3),...
        kepE(4),kepE(5),kepE(6),muE);       % Earth pos in HelioEclipFr XYZ

    % Unit vector pointing from Sun to s/c in Geocentric eq fr, IJK
    epsilon = (23+45/60)*pi/180;        % Earth equator/ecliptic tilt [rad]
    rE = xEulerRotFr(-epsilon)*rE';     % Earth pos in GeoEqFr IJK
    r_sc_S  = [rE(1)+rx,rE(2)+ry,rE(3)+rz]; 
    u_sc_S  = r_sc_S/norm(r_sc_S);

    % Solar Radiation Pressure perturbation
    rS    = [-rE(1),-rE(2),-rE(3)]; % Sun position GeoEqFr IJK
    rSC   = [rx,ry,rz];             % s/c position GeoEqFr IJK
    thP   = acos(dot(rS,rSC)/(norm(rS)*norm(rSC)));
    th1   = acos(6378/norm(rSC));
    th2   = acos(6378/norm(rS));
    if th1+th2 <= thP
        nu = 0;
    else
        nu = 1;
    end
    PSR     = 4.56e-6;          % Solar radiation pressure [N/m2]
    pSR     = nu*PSR*CR*Am/1000;% Solar radiation pert magnitude [kg/s2]
    a_PSR   = pSR*u_sc_S;       % Solar radiation pert accel GeoEqFr IJK

    % Sum of each contribution 
    ap      = a_J2 + a_PSR;     % perturbation acc GeoEqFr IJK

    % Projection on non-inertial tangential-normal coordinates tnh
    t       = v'/norm(v);                       % �_v
    h       = cross(r,v)'/norm(cross(r,v));     % �_h
    n       = cross(h,t);                       % �_n

    at      = ap*t;                             % �_theta comp of pert acc
    an      = ap*n;                             % �_h comp of pert acc
    ah      = ap*h;                             % �_r comp of pert acc
    
end