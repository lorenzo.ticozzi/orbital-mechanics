function represent2(long_c,lat_c,long_g,lat_g,t_c,r_c,v_c,...
    t_g,e_g,a_g,i_g,RAAN_g,w_g,theta_g)

% Preprocessing
    [e_c,a_c,i_c,RAAN_c,w_c,theta_c] = statevec2OE(r_c,v_c);

%% Represent track

    % 2D map load:
%     figure
%     axes1 = axes;
%     addpath([pwd,'/m_map'])
%     m_proj('Equidistant Cylindrical','lat',90);
%     m_coast('color',[0 .6 0]);
%     m_grid('linestyle','none','box','fancy','tickdir','out');
%     set(axes1,'NextPlot','add', 'Visible','off')
%     hold on
% 
%     long_c(long_c>180) = -(360-long_c(long_c>180));
%     p1 = plot(long_c*pi/180,lat_c*pi/180,'.','MarkerSize',1);
%     long_g(long_g>180) = -(360-long_g(long_g>180));
%     p2 = plot(long_g*pi/180,lat_g*pi/180,'.','MarkerSize',1);
%     legend([p1 p2],{'Cartesian coordinates','Gauss: orbital elements'})

    figure
    xlabel('Longitude [�]');
    ylabel('Latitutde [�]');
    axis([-180 180 -90 90]);
    Earth = image('CData',imread('Earth.jpg'));
    Earth.XData = [-180, 180];
    Earth.YData = [90, -90];
    hold on;
    grid on;

    long_c(long_c>180) = -(360-long_c(long_c>180));
    p1 = plot(long_c,lat_c,'.','MarkerSize',1);
    long_g(long_g>180) = -(360-long_g(long_g>180));
    p2 = plot(long_g,lat_g,'.','MarkerSize',1);
    legend([p1 p2],{'Cartesian coordinates','Gauss: orbital elements'})


%% Represent orbital elements
    figure
    
    subplot(2,3,1) 
    hold on
    plot(t_c/3600/24,e_c,t_g/3600/24,e_g)
    xlabel('Time [days]')
    ylabel('Eccentricity')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on

    subplot(2,3,2), 
    hold on
    plot(t_c/3600/24,a_c,t_g/3600/24,a_g)
    xlabel('Time [days]')
    ylabel('Semimajor Axis (Km)')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on

    subplot(2,3,3)
    hold on
    plot(t_c/3600/24,RAAN_c*180/pi,t_g/3600/24,RAAN_g*180/pi)
    xlabel('Time [days]')
    ylabel('RAAN (�)')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on

    subplot(2,3,4)
    hold on
    plot(t_c/3600/24,w_c*180/pi,t_g/3600/24,w_g*180/pi)
    xlabel('Time [days]')
    ylabel('Argument of Perigee (�)')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on

    subplot(2,3,5)
    hold on
    plot(t_c/3600/24,i_c*180/pi,t_g/3600/24,i_g*180/pi)
    xlabel('Time [days]')
    ylabel('Inclination (�)')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on

    subplot(2,3,6)
    hold on
    plot(t_c/3600/24,theta_c*180/pi,t_g/3600/24,theta_g*180/pi)
    xlabel('Time [days]')
    ylabel('Anomaly (�)')
    legend('Cartesian coordinates','Gauss: orbital elements')
    grid on