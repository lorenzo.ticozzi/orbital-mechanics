% main_planetary_explorer_mission.m - Explorer mission around Earth
%
% PROTOTYPE:
%
%
% DESCRIPTION: 
%   Explorer mission around Earth. 
%   The orbit:
%       a = 39062 km, e = 0.3537, i = 26.3597 deg, hp = 18867.6 km
%   is being perturbated by:
%       - J2 
%       - SRP (cR = 1, A/m = 0.1 m^2/kg) 
%
%
% INPUT :
%
%
% OUTPUT:
%
%
% FUNCTIONS CALLED:
%
%
% AUTHORS:
%   - Michele Maranesi
%   - Lorenzo Ticozzi
%   - Salvador Ribes Izquierdo
%
% PREVIOUS VERSION:
%   
%
% CHANGELOG:
%   
%
% -------------------------------------------------------------------------
clear all; close all; clc;

% addpath /Users/salvadorribes/Documents/MATLAB/timeConversion
% addpath /Users/salvadorribes/Documents/MATLAB/m_map
% addpath /Users/salvadorribes/Documents/MATLAB/textures

load('data.mat')

% Settings
nd         = 50;                % n of days
ODEoptions = odeset('AbsTol',1e-10,'RelTol',1e-10); % ODE options

% Global constants
G   = astroConstants(1);        % Universal gravity constant [km^3/kg/s^2)]
rM  = astroConstants(7);        % Earth-Moon mean distance [km]
gE  = astroConstants(9);        % Earth gravitational constant [km^3/s^2]
muE = astroConstants(13);       % Earth gravitaitonal parameter [km^3/s^2]
RE  = astroConstants(23);       % Earth mean radius [km]
OmE = (2*pi+2*pi/365.26)/24/3600;% Earth rate of rotation [rad/s]

% Perturbation data
J2  = 0.00108263;               % Earth's second zonal harmonic
CR  = 1;                        % Radiation Pressure Coefficient (1<CR<2)
Am  = 0.1;                      % Satellite's area2mass ratio [m2/kg]

% Initial conditions of s/c
jd0     = date2mjd2000([2018,01,01,00,00,00]);% Julian date [days(mjd2000)]
GST0    = 0;                    % Greenwich Sideral Time [rad]
e0      = 0.3537;               % Eccentricity of original orbit [-]
a0      = 39062;                % Semimajor axis [km]       
i0      = 26.3597*pi/180;       % Inclination [rad]
RAAN0   = 90*pi/180;            % Righ Ascension of Ascending Node [rad]
w0      = 45*pi/180;            % Argument of perigee [rad]      8
theta0  = 120*pi/180;           % True anomaly [rad]

% Time
T       = 2*pi*sqrt(a0^3/muE);  % Orbit period
n       = (nd*24*3600)/T;       % n� of turns
tspan   = 0:(T/2500):(n*T);     % time vector

% Parameters vector
parameters = [muE,RE,J2,OmE,GST0,Am,jd0,CR];  

% Initial State vector (r,v)
[r0,v0] = OE2statevec(e0,a0,i0,RAAN0,w0,theta0,muE);  % r,v GeoEqFr IJK  

% Dynamic motion (Cartesian coordinates GeoEqFr IJK)
[t_c,r_c,v_c] = eqsMotion(r0,v0,parameters,ODEoptions,tspan);
[e_c,a_c,i_c,RAAN_c,w_c,theta_c] = statevec2OE(r_c,v_c);

% Dynamic motion (Orbital elements: Gauss equations)
[t_g,e_g,a_g,i_g,RAAN_g,w_g,theta_g] = ...
    eqsGauss(e0,a0,i0,RAAN0,w0,theta0,parameters,ODEoptions,tspan);
[r_g,v_g] = OE2statevec(e_g,a_g,i_g,RAAN_g,w_g,theta_g,muE);

% % Represent the orbit in 3D
% figure(1)
% drawPlanet('Earth',23,[0,0,0]);
% hold on
% p1 = plot3(r_c(:,1),r_c(:,2),r_c(:,3),'b');
% hold on
% p2 = plot3(r_g(:,1),r_g(:,2),r_g(:,3),'r');
% axis equal
% xlabel('X[km]')
% ylabel('Y[km]')
% zlabel('Z[km]')
% legend([p1 p2],{'Cartesian coordinates','Gauss: orbital elements'})

% Ground track
[~,~,lat_c,long_c] = groundtrack(r_c,GST0,OmE,t_c);
[~,~,lat_g,long_g] = groundtrack(r_g,GST0,OmE,t_g);

% Represent orbit track
represent2(long_c,lat_c,long_g,lat_g,t_c,r_c,v_c,...
   t_g,e_g,a_g,i_g,RAAN_g,w_g,theta_g)

% %%%% Comparison to real satellite data %%%
% 
% % Initial orbital elements and date
% jd0      = date2mjd2000([2018,01,01,00,00,00]); 
% GST0     = 0;                                   % rad
% e0R      = e_real(1);
% a0R      = a_real(1);
% i0R      = i_real(1)*pi/180;
% RAAN0R   = Omega_real(1)*pi/180;
% w0R      = omega_real(1)*pi/180;
% theta0R  = theta_real(1)*pi/180;
% 
% % Update vector of parameters
% parameters = [muE,RE,J2,OmE,GST0,Am,jd0,CR];
% 
% % Initial State vector (r,v) of a Real Satellite
% [r0R,v0R] = OE2statevec(e0R,a0R,i0R,RAAN0R,w0R,theta0R,muE); % r,v GeoEqFr IJK
% 
% % Cartesian equations propagation
% [t_cR,r_cR,v_cR] = eqsMotion(r0R,v0R,parameters,ODEoptions,tspan);
% [e_cR,a_cR,i_cR,RAAN_cR,w_cR,theta_cR] = statevec2OE(r_cR,v_cR);
% 
% % Gauss equations propagation
% [t_gR,e_gR,a_gR,i_gR,RAAN_gR,w_R,theta_gR] = ...
%     eqsGauss(e0R,a0R,i0R,RAAN0R,w0R,theta0R,parameters,ODEoptions,tspan);
% 
% figure
% subplot(2,3,1), hold on
% p1 = plot(t_c,e_c);
% p2 = plot(t_g,e_g);
% p3 = plot(t_cR,e_cR);
% p4 = plot(t_gR,e_gR);
% xlabel('Time [s]')
% ylabel('Eccentricity')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;
% 
% subplot(2,3,2), 
% hold on;
% p1 = plot(t_c,a_c);
% p2 = plot(t_g,a_g);
% p3 = plot(t_cR,a_cR);
% p4 = plot(t_gR,a_gR);
% xlabel('Time [s]')
% ylabel('Semimajor Axis (Km)')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;
% 
% subplot(2,3,3), 
% hold on;
% p1 = plot(t_c,RAAN_c*180/pi);
% p2 = plot(t_g,RAAN_g*180/pi);
% p3 = plot(t_cR,RAAN_cR*180/pi);
% p4 = plot(t_gR,RAAN_gR*180/pi);
% xlabel('Time [s]')
% ylabel('RAAN (�)')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;
% 
% subplot(2,3,4), 
% hold on;
% p1 = plot(t_c,w_c*180/pi);
% p2 = plot(t_g,w_g*180/pi);
% p3 = plot(t_cR,w_cR*180/pi);
% p4 = plot(t_gR,w_R*180/pi);
% xlabel('Time [s]')
% ylabel('Argument of Perigee (�)')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;
% 
% subplot(2,3,5), 
% hold on;
% p1 = plot(t_c,i_c*180/pi);
% p2 = plot(t_g,i_g*180/pi);
% p3 = plot(t_cR,i_cR*180/pi);
% p4 = plot(t_gR,i_gR*180/pi);
% xlabel('Time [s]')
% ylabel('Inclination (�)')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;
% 
% subplot(2,3,6), 
% hold on;
% p1 = plot(t_c,theta_c*180/pi);
% p2 = plot(t_g,theta_g*180/pi);
% p3 = plot(t_cR,theta_cR*180/pi);
% p4 = plot(t_gR,theta_gR*180/pi);
% xlabel('Time [s]')
% ylabel('Anomaly (�)')
% legend([p1 p2 p3 p4],...
%     'Cartesian coordinates, exercise',...
%     'Gauss: orbital elements, exercise',...
%     'Cartesian coordinates, real',...
%     'Gauss: orbital elements, real')
% grid on;


