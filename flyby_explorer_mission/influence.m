
% -------------------------------------------------------------------------
function [r_SOI] = influence(muP,R)
% ------------------------------------------------------------------------- 
%
% PROTOTYPE        r_SOI = influence(muP,R)
%
% DESCRIPTION      The following function computes the radius of the sphere
%                  of influence of a given planet in the solar system.
%
% INPUT            muP  -  Planetary constant of the planet of interest 
%                          [km^3/s^2]
%                  R    -  Mean distance from the Sun [km/s]
%
% OUTPUT           r_SOI  -  value of the radius of the sphere of influence
%                            (SOI) [km]
%
% FUNCTIONS CALLED:        ---------
%
% AUTHOR:                 Michele Maranesi, Lorenzo Maria Ticozzi and 
%                         Salvador Ribes, 26/07/2018, MATLAB
%   
% CHANGELOG:              ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------


global muS

r_SOI = R * (muP/muS)^(2/5);


return











