
%--------------------------------------------------------------------------
function [DV,dep,arr,opt_tof,xdates,ydates,row,col] = porkchop(early_dep,late_dep,early_arr,late_arr,planets_ID,t_step_dep,t_step_arr)
%--------------------------------------------------------------------------

% pork_chop.m - Pork chop plot for the transfer between selected planets, 
%               with given departure and arrival date intervals.
%
% PROTOTYPE  [DV,dep,arr] = porkchop(early_dep,late_dep,early_arr,late_arr,planets_ID,mu)
%
% DESCRIPTION    The following function creates the 'porkchop' plot for a
%                transfer between two different planets identified by their
%                ID in the call of the function. The user should provide as
%                inputs the departure and arrival windows for the mission
%                and would get the best total DV necessary to perform the
%                transfer together with the optimal departure and arrival
%                dates and the time of flight.
%
% INPUT          early_dep = [yyyy mm dd HH MM SS]  -  early departure date
%                late_dep = [yyyy mm dd HH MM SS]  -  late departure date
%                early_arr = [yyyy mm dd HH MM SS]  -  early arrival date
%                late_arr = [yyyy mm dd HH MM SS]  -  late arrival date
%
%                planets_ID = [ID1 ID2]  -  ID1 -> departure planet
%                                           ID2 -> arrival planet
%
%                                           1:   Mercury
%                                           2:   Venus
%                                           3:   Earth
%                                           4:   Mars
%                                           5:   Jupiter
%                                           6:   Saturn
%                                           7:   Uranus
%                                           8:   Neptune
%                                           9:   Pluto
%                                           10:  Sun
%             
%                t_step_dep  - time step that spans the departure window.
%                              The most refined step is 1 day (min value) 
%
%                t_step_arr  -  time step that spans the arrival window
%
%     
%               
% OUTPUT         DV  -  total delta velocity required to perform the best
%                       transfer
%                
%                dep  -  best departure date
%
%                arr  -  best arrival date
%
%                opt_tof  -  time of flight corresponding to the best 
%                            transfer found with the function
%
% CONTRIBUTORS
%
%  Lorenzo Ticozzi
%  Michele Maranesi
%  Salvador Ribes

% - Definitions of global variables 
global orbitType Nrev Ncase optionsLMR muS Nfigure


% - Check on the IDs of the planets
if planets_ID(1) == planets_ID(2)
    error('Choose different IDs for the two planets')
end

switch planets_ID(1)
    case 1
        planet1 = 'Mercury';
    case 2
        planet1 = 'Venus';
    case 3
        planet1 = 'Earth';
    case 4
        planet1 = 'Mars';
    case 5
        planet1 = 'Jupiter';
    case 6
        planet1 = 'Saturn';
    case 7
        planet1 = 'Uranus';
    case 8
        planet1 = 'Neptune';
    case 9
        planet1 = 'Pluto';
    otherwise
        error('Wrong ID for departure planet')
end

switch planets_ID(2)
    case 1
        planet2 = 'Mercury';
    case 2
        planet2 = 'Venus';
    case 3
        planet2 = 'Earth';
    case 4
        planet2 = 'Mars';
    case 5
        planet2 = 'Jupiter';
    case 6
        planet2 = 'Saturn';
    case 7
        planet2 = 'Uranus';
    case 8
        planet2 = 'Neptune';
    case 9
        planet2 = 'Pluto';
    otherwise
        error('Wrong ID for arrival planet')
end


% - Transformation of the dates in mjd2000 days to be used in 'uplanet.m'
dep_early = date2mjd2000(early_dep);
dep_late = date2mjd2000(late_dep);

arr_early = date2mjd2000(early_arr);
arr_late = date2mjd2000(late_arr);


% - Creation of the time vectors
dep_vec = [dep_early : t_step_dep : dep_late];
arr_vec = [arr_early : t_step_arr : arr_late];

n = length(dep_vec);
m = length(arr_vec);

h = waitbar(0,sprintf('Calculating porkchop between %s and %s', planet1,...
    planet2));

% - Initialize DV storage matrix
DV_mat = zeros(m,n);
tof_mat = zeros(m,n);

for i = 1 : n
    for j = 1 : m
        dep_day = dep_vec(i);
        arr_day = arr_vec(j);
        
        % - Check that the arrival day is after than the departure day...
        if arr_day <= dep_day
            DV_mat(j,i) = NaN;
            tof_mat(j,i) = NaN;
            continue
        end
        
        % - Matrices with collection of dates
        dep_mat(i,:) = mjd20002date(dep_day);
        arr_mat(j,:) = mjd20002date(arr_day);
        
        tof_days = arr_day - dep_day;
        tof = tof_days * 86400;
        tof_mat(j,i) = tof_days;
        
        % - Positions of the planets at departure and arrival
        [kep1,muS] = uplanet(dep_day,planets_ID(1));
        [r1,v1] = kep2car(kep1(1),kep1(2),kep1(3),kep1(4),...
            kep1(5),kep1(6),muS);
        
        [kep2,muS] = uplanet(arr_day,planets_ID(2));
        [r2,v2] = kep2car(kep2(1),kep2(2),kep2(3),kep2(4),...
            kep2(5),kep2(6),muS);
        
        
        % - Solution of the Lambert arc in the given position with the
        % given tof
        [~,~,~,~,VI,VF,~,~] = lambertMR(r1,r2,tof,muS,orbitType,Nrev,...
            Ncase,optionsLMR);
        
        % - Computation of the corresponding DV
        DV1 = norm(VI - v1);
        DV2 = norm(v2 - VF);
        DV_tot = DV1 + DV2;
        
        % - Upper limit for the DV to be plotted
        if DV_tot >= 20
            DV_mat(j,i) = NaN;
        else
            DV_mat(j,i) = DV_tot;
        end
    end
    waitbar(i/n)
end

% - Plot of the 'porkchop'

xdates = datenum(dep_mat(:,1),dep_mat(:,2),dep_mat(:,3));
ydates = datenum(arr_mat(:,1),arr_mat(:,2),arr_mat(:,3));

Nfigure = Nfigure +1;
figure(Nfigure)
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

contourf(xdates,ydates,DV_mat,15);
datetick('x','yyyy-mmm-dd','keepticks')
xtickangle(45)
datetick('y','yyyy-mmm-dd','keepticks')

xlabel('Departure dates','Interpreter','latex')
ylabel('Arrival dates','Interpreter','latex')
title(sprintf('Porkchop plot of the transfer from %s to %s',planet1,...
    planet2),'Interpreter','latex')

c = contourcbar('eastoutside','TickLabelInterpreter','latex');
c.Label.String = '\DeltaV [km/s]';
axis equal

hold on

% - Get the best solution and plot it
[min_val, ID] = min(DV_mat(:));
[row,col] = ind2sub(size(DV_mat),ID);
DV = min_val;

plot(xdates(col),ydates(row),'Marker','o','MarkerEdgeColor','r',...
    'MarkerFaceColor','r','MarkerSize',8)
hold on

% - Plot of the tof over the Porkchop
caxis([DV,20])
[c,h] = contour(xdates,ydates,tof_mat,4);
h.LineColor = 'red';
h.LineWidth = 1.5;
h.ShowText = 'on';

% - Add legend to the contour
lgd = legend('\DeltaV [km/s]','Cheapest transfer','Time of flight [days]',...
    'Location','best');
set(lgd,'Interpreter','tex','Autoupdate','off')

% - Optimal departure and arrival dates
dep = dep_mat(col,:);
arr = arr_mat(row,:);

% - Optimal time of flight
opt_tof = date2mjd2000(arr) - date2mjd2000(dep);

return


















