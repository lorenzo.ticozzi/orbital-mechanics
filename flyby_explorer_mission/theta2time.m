function delta_t = theta2time(a,e,mu,theta_i,theta_f)

% theta2time.m - gives the time between two different positions on a
% certain orbit
%
% PROTOTYPE: delta_t = theta2time(theta_i,theta_f,orbitType)
%
% DESCRIPTION: The following function implements the computation of the
%              time needed to go from theta_i to theta_f on a certain orbit
%              defined by its a,e. Each true anomaly theta is measured from
%              the perigee, in which the time is supposed to be zero. 
% 
% INPUT: a                 Semi-major axis of the orbit
%        e                 Eccentricity of the orbit
%        h                 Angular momentum of the orbit
%        mu                Gravitational constant of the main attractor
%        theta_i [rad]     Initial true anomaly on the orbit
%        theta_f [rad]     Final true anomaly on the orbit
%        
% OUTPUT: delta_t [s]      Time of flight between theta_i and theta_f
%
%--------------------------------------------------------------------------


if nargin < 5
    error ('Not enough input arguments')
end

% if theta_f < theta_i
%     error ('Theta_f must be greater or at least equal to theta_i')
% end

% P parameter for the orbit
p = a*(1-e^2);

if (0 <= e) && (e < 1)   % elliptic orbit
    
    E_i = 2*atan(sqrt((1-e)/(1+e))*tan(theta_i/2));  % Eccentric anomalies
    if E_i < 0                                      % Check on the angle to avoid negative results
        E_i = E_i + 2*pi;
    end
    
    E_f = 2*atan(sqrt((1-e)/(1+e))*tan(theta_f/2));
    if E_f <= 0
        E_f = E_f + 2*pi;
    end
    
    t_i = sqrt(a^3/mu)*(E_i-e*sin(E_i));             % Times from perigee
    t_f = sqrt(a^3/mu)*(E_f-e*sin(E_f));
    
    delta_t = t_f - t_i;                             % Time of flight
    
elseif e > 1  % hyperbolic orbit
    
    F_i = 2*atanh(sqrt((e-1)/(e+1))*tan(theta_i/2)); % Eccentric anomalies
    if F_i < 0
        F_i = F_i + 2*pi;
    end
    
    F_f = 2*atanh(sqrt((e-1)/(e+1))*tan(theta_f/2));
    if F_f <= 0
        F_f = F_f + 2*pi;
    end
    
    t_i = sqrt(abs(a)^3/mu)*(e*sinh(F_i)-F_i);       % Times from perigee
    t_f = sqrt(abs(a)^3/mu)*(e*sinh(F_f)-F_f);
    
    delta_t = t_f - t_i;                             % Time of flight

elseif e == 1  % parabolic orbit
    
    D_i = tan(theta_i/2);                            % Eccentric anomalies
    D_f = tan(theta_f/2);
    
    t_i = sqrt(p^3/mu)*1/2*(D_i+(D_i^3)/3);          % Times from perigee
    t_f = sqrt(p^3/mu)*1/2*(D_f+(D_f^3)/3);
    
    delta_t = t_f - t_i;                             % Time of flight
    
else
    error('Wrong value of e')
end
    

end










