% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% ~                                                                     ~ %
% ~                                                                     ~ %
% ~              ASSIGNMENT 1: FLYBY EXPLORER MISSION                   ~ %
% ~                                                                     ~ %
% ~                                                                     ~ %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ % 


% - INTRODUCTION - 
%
% The following script is the main script of the first assignment (Flyby 
% Explorer Mission), and describes the design of a trajectory to go from 
% Neptune to Jupiter, performing a mid-way powered gravity assist around 
% Uranus. 

% - INDEX -
%
% - 0 DEFINITION OF GLOBAL VARIABLES

% - 1 OPTIMIZATION OF THE TIME WINDOWS
% -   1.1 Porkchop plot for Lambert arcs
% -   1.2 Genetic algorithm optimization ('ga' function)
% -   1.3 Triple 'for' cycle

% - 2 PLOT OF THE STRATEGY
% -   2.1 Solar System
% -   2.2 Flyby Explorer Mission
% -   2.3 Flyby around Uranus

% - WARNING -
% The drawPlanet.m function must be adapted to the computer running this
% script; the path leading to the 'textures' folder must be changed to make
% the script run properly.


%% ======================== 0 - GLOBAL VARIABLES  ======================== %%

global ID_N ID_U ID_J
ID_N = 8; ID_U = 7; ID_J = 5;

global muU muS hatm radU RU
muU = astroConstants(17);  muS = astroConstants(4); hatm = 4000;
radU = astroConstants(27);  RU = 2.871e+9;

global orbitType Nrev Ncase optionsLMR
orbitType = 0; Nrev = 0; Ncase = 0; optionsLMR = 2;

global period_N period_U period_J
period_N = 60182; period_U = 30688.5; period_J = 4332.59;

global tstep_N tstep_U tstep_J
tstep_N = floor(period_N / (360)); 
tstep_U = floor(period_U / (360));
tstep_J = floor(period_J / (360));

global Nfigure
Nfigure = 0; 

global scaleN scaleU scaleJ
scaleN = 5e+3; scaleU = 5e+3; scaleJ = 2.5e+3;

%% ============   1.1  PORKCHOP PLOTS for the LAMBERT ARCS   =========== %%

% In this section two porkchop plots are plotted to understand the 
% behaviour of the launch windows. The time period over which the first 
% porkchop is plotted is computed keeping in mind the synodic period of 
% Uranus wrt to Neptune, so that significant data are obtained. The second
% porkchop is instead computed with the departure window from Uranus that 
% is around the optimal arrival time computed with the first porkchop.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
format long
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% - First porkchop: Neptune -> Uranus

early_depN = [2020 01 01 0 0 0];
late_depN = [2200 12 31 0 0 0];

early_arrU = [2030 01 01 0 0 0];
late_arrU = [2300 12 31 0 0 0];

planets_ID1 = [ID_N ID_U];

[DV_NU,depN,arrU,tofNU,xdates1,ydates1,row1,col1] = porkchop(early_depN,...
    late_depN,early_arrU,late_arrU,planets_ID1,tstep_N,tstep_U);
hold on

fprintf ('The porkchop from Neptune to Uranus identifies %d %d %d as best departure date and %d %d %d as best arrival date to Uranus \n',depN(1:3),arrU(1:3))

% - Highlight the window over which the 'ga' will be used

galimit_low_dep = [2080 01 01 0 0 0];
galimit_high_dep = [2160 12 31 0 0 0];
galimit_low_flyby = 8000;
galimit_high_flyby = 1.5e+4;

marginx_inf = date2mjd2000(depN) - date2mjd2000(galimit_low_dep);
marginx_sup = date2mjd2000(galimit_high_dep) - date2mjd2000(depN);


xlimit1 = [xdates1(col1)-marginx_inf xdates1(col1)-marginx_inf];
xlimit2 = [xdates1(col1)+marginx_sup xdates1(col1)+marginx_sup]; 
ylimit = [ydates1(end) ydates1(1)];

ylimit1 = [ydates1(row1)-galimit_low_flyby ydates1(row1)-...
    galimit_low_flyby];
ylimit2 = [ydates1(row1)+galimit_high_flyby ydates1(row1)+...
    galimit_high_flyby];
xlimit = [xdates1(end) xdates1(1)];

% - Plot boundary lines

earlydep = line(xlimit1,ylimit);       latedep = line(xlimit2,ylimit);
earlydep.Color = 'yellow';             latedep.Color = 'yellow';
earlydep.LineWidth = 2.5;              latedep.LineWidth = 2.5;
earlydep.LineStyle = '--';             latedep.LineStyle = '--';

earlyflyby = line(xlimit,ylimit1);      lateflyby = line(xlimit,ylimit2);
earlyflyby.Color = 'yellow';            lateflyby.Color = 'yellow';
earlyflyby.LineWidth = 2.5;             lateflyby.LineWidth = 2.5;
earlyflyby.LineStyle = '--';            lateflyby.LineStyle = '--';

hold off

%%
% - Second porkchop: Uranus -> Jupiter
early_depU = [2195 01 01 0 0 0];
late_depU = [2235 12 31 0 0 0];

early_arrJ = [2205 01 01 0 0 0];
late_arrJ = [2250 12 31 0 0 0];

planets_ID2 = [ID_U ID_J];

[DV_UJ,depU,arrJ,tofUJ,xdates2,ydates2,row2,col2] = porkchop(early_depU,...
    late_depU,early_arrJ,late_arrJ,planets_ID2,tstep_U,tstep_J);

fprintf('\n The porkchop from Uranus to Jupiter identifies %d %d %d as best departure date and %d %d %d as best arrival date to Uranus \n',depU(1:3),arrJ(1:3))

% - Highlight the window over which the 'ga' will be used

galimit_low_arr = 1e+3;
galimit_high_arr = 2e+3;

ylimit3 = [ydates2(row2)-galimit_low_arr ydates2(row2)-galimit_low_arr];
ylimit4 = [ydates2(row2)+galimit_high_arr ydates2(row2)+galimit_high_arr];
xlimit = [xdates2(end) xdates2(1)];

earlyarr = line(xlimit,ylimit3);       latearr = line(xlimit,ylimit4);
earlyarr.Color = 'yellow';             latearr.Color = 'yellow';
earlyarr.LineWidth = 2.5;              latearr.LineWidth = 2.5;
earlyarr.LineStyle = '--';             latearr.LineStyle = '--';


%% ==============  1.2 GENETIC ALGORITHM OPTIMIZATION  ================= %%

% In this section, starting from the time windows trimmed in the previous
% section thanks to the inspection of the porkchops, the 'ga' function of
% Matlab is used to further reduce the departure, flyby and arrival
% windows. ga function tries to optimize the total DV (DV.m function).
% Once that three smaller time windows are obtained looking at the results
% of multiple uses of ga, a triple 'for' cycle is used to get the best
% result in a deterministic way.

% - First ga cycle with time windows coming from porkchops

short_TOF1 = tofNU - galimit_low_flyby;
long_TOF1 = tofNU + galimit_high_flyby;

short_TOF2 = tofUJ - galimit_low_arr;
long_TOF2 = tofUJ + galimit_high_arr;

LB1 = [date2mjd2000(galimit_low_dep);
    short_TOF1;
    short_TOF2];

UB1 = [date2mjd2000(galimit_high_dep);
    long_TOF1;
    long_TOF2];

ga_times1 = 10;
dates_mat1 = zeros(ga_times1,3);
DV_vec1 = zeros(ga_times1,1);
departures1 = zeros(ga_times1,6);
flybys1 = zeros(ga_times1,6);
arrivals1 = zeros(ga_times1,6);

for i = 1 : ga_times1
    % - Run the ga
    [opt_dates1, fval1, ~] = ga_dates(LB1,UB1); 
    
    % - Save the date...
    dates_mat1(i,:) = opt_dates1;
    departures1(i,:) = mjd20002date(dates_mat1(i,1));       
    flybys1(i,:) = mjd20002date(dates_mat1(i,1)+dates_mat1(i,2));     
    arrivals1(i,:) = mjd20002date(dates_mat1(i,1)+dates_mat1(i,2)+...
        dates_mat1(i,3));
    
    % - Vector of DV
    DV_vec1(i,1) = fval1;
end

% - Find the best solutions between the ones computed by the first ga

best_DV11 = min(DV_vec1)
best_i1 = find(DV_vec1 == best_DV11);

best_TOF11 = dates_mat1(best_i1,2);
best_TOF21 = dates_mat1(best_i1,3);

best_dep_date1 = departures1(best_i1,:);
best_flyby_date1 = flybys1(best_i1,:);
best_arr_date1 = arrivals1(best_i1,:);

fprintf('The best solution found by the first ga cycle is:\n %d %d %d as departure date \n', best_dep_date1(1:3))
fprintf('%d %d %d as flyby date \n', best_flyby_date1(1:3))
fprintf('%d %d %d as arrival date \n', best_arr_date1(1:3))

%%
% - Second ga cycle: the time windows are trimmed using the results of the
% first ga cycle

% - Add or subtract days to the best solution from the first ga
margin_DATES = [0002 00 00 0 0 0];
margin_TOF1 = 500;
margin_TOF2 = 400;

early_depga = best_dep_date1 - margin_DATES;
early_TOF1 = best_TOF11 - margin_TOF1;
early_TOF2 = best_TOF21 - margin_TOF2;

late_depga = best_dep_date1 + margin_DATES;
late_TOF1 = best_TOF11 + margin_TOF1;
late_TOF2 = best_TOF21 + margin_TOF2;

LB2 = [date2mjd2000(early_depga);
    early_TOF1;
    early_TOF2];

UB2 = [date2mjd2000(late_depga);
    late_TOF1;
    late_TOF2];

ga_times2 = 15;

% - Initialize quantities
dates_mat2 = zeros(ga_times2,3);
DV_vec2 = zeros(ga_times2,1);
departures2 = zeros(ga_times2,6);
flybys2 = zeros(ga_times2,6);
arrivals2 = zeros(ga_times2,6);

for i = 1 : ga_times2
    % - Run the ga
    [opt_dates2, fval2, ~] = ga_dates(LB2,UB2);
    
    % - Save the date...
    dates_mat2(i,:) = opt_dates2;
    departures2(i,:) = mjd20002date(dates_mat2(i,1));
    flybys2(i,:) = mjd20002date(dates_mat2(i,1)+dates_mat2(i,2));
    arrivals2(i,:) = mjd20002date(dates_mat2(i,1)+dates_mat2(i,2)+...
        dates_mat2(i,3));
    
    % - Vector of DV
    DV_vec2(i,1) = fval2;
end


% - Find the best solution out of the second ga cycle

best_DV12 = min(DV_vec2)
best_i2 = find(DV_vec2 == best_DV12);

best_TOF12 = dates_mat2(best_i2,2);
best_TOF22 = dates_mat2(best_i2,3);

best_dep_date2 = departures2(best_i2,:);
best_flyby_date2 = flybys2(best_i2,:);
best_arr_date2 = arrivals2(best_i2,:);

fprintf('The best solution found by the second ga cycle is:\n %d %d %d as departure date \n', best_dep_date2(1:3))
fprintf('%d %d %d as flyby date \n', best_flyby_date2(1:3))
fprintf('%d %d %d as arrival date \n', best_arr_date2(1:3))

%% ======================= 1.3 TRIPLE 'FOR' CYCLE ====================== %%

% Once the porkchops and the use of genetic algorithm have found more
% precises time windows, a triple 'for' cycle can be used to identify the
% best departure, flyby and arrival dates in a deterministic way.

% - Add or subtract days to the best solution from the second ga
margin2_departure = [0002 00 00 0 0 0];
margin2_flyby = [0003 00 00 0 0 0];
margin2_arrival = [0004 00 00 0 0 0];

% tstep_N = tstep_N/8;
% tstep_U = tstep_U/8;
% tstep_J = tstep_J/8;

early_dep = best_dep_date2 - margin2_departure;
late_dep = best_dep_date2 + margin2_departure;

early_flyby = best_flyby_date2 - margin2_flyby;
late_flyby = best_flyby_date2 + margin2_flyby;

early_arr = best_arr_date2 - margin2_arrival;
late_arr = best_arr_date2 + margin2_arrival;

[dep_date,flyby_date,arr_date,best_TOF1,best_TOF2,minDV] = ...
    triple4(early_dep,late_dep,early_flyby,late_flyby,early_arr,late_arr)

fprintf('The best solution found by the triple for cycle is:\n %d %d %d as departure date \n', dep_date(1:3));
fprintf('%d %d %d as flyby date \n', flyby_date(1:3));
fprintf('%d %d %d as arrival date \n', arr_date(1:3));

%% ========================== 2.1 SOLAR SYSTEM ========================= %%

% Using the dates that come out from the triple 'for' cycle, we can now
% plot the exact position of our planets during the explorer mission

% - Plot 3
Nfigure = Nfigure +2; 
figN1 = plotplanets(ID_N,dep_date,1,'Blue',scaleN,Nfigure);
hold on
grid on
figU1 = plotplanets(ID_U,flyby_date,0,'Cyan',scaleU,Nfigure);
hold on
figJ1 = plotplanets(ID_J,arr_date,0,'Yellow',scaleJ,Nfigure);
xlabel('X axis [km]','Interpreter','latex')
ylabel('Y axis [km]','Interpreter','latex')
zlabel('Z axis [km]','Interpreter','latex')
axis equal
hold off
title('Plot of "Solar System"','Interpreter','latex')
legend('Neptune','Neptune''s orbit','Sun','Uranus','Uranus''s orbit',...
    'Jupiter','Jupiter''s orbit')


%% ===================== 2.2 FLYBY EXPLORER MISSION ==================== %%

% - Definition of position and velocity for every planet in their crucial
%   position
STARTdate= date2mjd2000(dep_date);
[kepN,ksunN] = uplanet(STARTdate, ID_N);
[rr_Neptune,vv_Neptune] =  kep2car(kepN(1),kepN(2),kepN(3),kepN(4),...
    kepN(5),kepN(6),ksunN);

FLYBYdate= date2mjd2000(flyby_date);
[kepU,ksunU] = uplanet(FLYBYdate, ID_U);
[rr_Uranus,vv_Uranus] =  kep2car(kepU(1),kepU(2),kepU(3),kepU(4),...
    kepU(5),kepU(6),ksunU);

ENDdate= date2mjd2000(arr_date);
[kepJ,ksunJ] = uplanet(ENDdate, ID_J);
[rr_Jupiter,vv_Jupiter] =  kep2car(kepJ(1),kepJ(2),kepJ(3),kepJ(4),...
    kepJ(5),kepJ(6),ksunJ);

[a_NU,P_NU,e_NU,ERROR_NU,v_transfer1_NU,v_transfer2_NU,TPAR_NU,THETA_NU] = ...
    lambertMR(rr_Neptune,rr_Uranus,best_TOF1*60*60*24,muS,orbitType,Nrev,Ncase,optionsLMR);
[a_UJ,P_UJ,e_UJ,ERROR_UJ,v_transfer1_UJ,v_transfer2_UJ,TPAR_UJ,THETA_UJ] = ...
    lambertMR(rr_Uranus,rr_Jupiter,best_TOF2*60*60*24,muS,orbitType,Nrev,Ncase,optionsLMR);

% - Plot 4
Nfigure = Nfigure +1;
figN2 = plotplanets(ID_N,dep_date,1,'Blue',scaleN,Nfigure,flyby_date);
hold on
grid on
figU2 = plotplanets(ID_U,dep_date,0,'Cyan',scaleU,Nfigure,flyby_date);
hold on
figJ2 = plotplanets(ID_J,arr_date,0,'Yellow',scaleJ,Nfigure);
hold on
lambert1 = plotlamberts(rr_Neptune,v_transfer1_NU,dep_date,...
    flyby_date,'Red',Nfigure);
xlabel('X axis [km]','Interpreter','latex')
ylabel('Y axis [km]','Interpreter','latex')
zlabel('Z axis [km]','Interpreter','latex')
axis equal
hold off
title('Analysis of FIRST Lambert''s problem','Interpreter','latex')
legend('Neptune''s path','Neptune''s orbit','Neptune','Sun',...
    'Uranus''s path','Uranus''s orbit','Uranus',...
    'Jupiter','Jupiter''s orbit','Lambert transfer Nep-Ura')

% - Plot 5
Nfigure = Nfigure +1;
figN3 = plotplanets(ID_N,dep_date,1,'Blue',scaleN,Nfigure);
hold on
grid on
figU3 = plotplanets(ID_U,flyby_date,0,'Cyan',scaleU,Nfigure,arr_date);
hold on
figJ3 = plotplanets(ID_J,flyby_date,0,'Yellow',scaleJ,Nfigure,arr_date);
hold on
lambert2 = plotlamberts(rr_Uranus,v_transfer1_UJ,flyby_date,...
    arr_date,'Red',Nfigure);
xlabel('X axis [km]','Interpreter','latex')
ylabel('Y axis [km]','Interpreter','latex')
zlabel('Z axis [km]','Interpreter','latex')
axis equal
hold off
title('Analysis of SECOND Lambert''s problem','Interpreter','latex')
legend('Neptune','Neptune''s orbit','Sun','Uranus''s path',...
    'Uranus''s orbit','Uranus','Jupiter''s path','Jupiter''s orbit',....
    'Jupiter','Lambert transfer Ura-Jup')

% - Plot 6
Nfigure = Nfigure +1;
figN4 = plotplanets(ID_N,dep_date,1,'Blue',scaleN,Nfigure);
hold on
grid on
figU4 = plotplanets(ID_U,flyby_date,0,'Cyan',scaleU,Nfigure);
hold on
figJ4 = plotplanets(ID_J,arr_date,0,'Yellow',scaleJ,Nfigure);
hold on
lambert1 = plotlamberts(rr_Neptune,v_transfer1_NU,dep_date,...
    flyby_date,'Red',Nfigure);
hold on
lambert2 = plotlamberts(rr_Uranus,v_transfer1_UJ,flyby_date,...
    arr_date,'Magenta',Nfigure);
xlabel('X axis [km]','Interpreter','latex')
ylabel('Y axis [km]','Interpreter','latex')
zlabel('Z axis [km]','Interpreter','latex')
axis equal
hold off
title('Plot of "Solar System"','Interpreter','latex')
legend('Neptune','Neptune''s orbit','Sun','Uranus','Uranus''s orbit',...
    'Jupiter','Jupiter''s orbit','Lambert transfer Nep-Ura',....
    'Lambert transfer Ura-Jup')

%% ====================== 2.3 FLYBY AROUND URANUS ====================== %%

% - Setting flyby analysis
Vinf_minus =  v_transfer2_NU - vv_Uranus;
Vinf_m = norm(Vinf_minus);
Vinf_plus  =  v_transfer1_UJ   - vv_Uranus;
Vinf_p = norm(Vinf_plus);

altitude_atm = 4000;
PossibleRings = 0;
R_MIN = radU + altitude_atm + PossibleRings;

% - Characterization of the 2 arcs of hyperbola
turn_angle = acos((dot(Vinf_minus,Vinf_plus))/(Vinf_m*Vinf_p));
r_SOI = influence(muU,RU);
x0 = [R_MIN r_SOI];
F1 = powered_ga(x0(1),turn_angle,muU,Vinf_m,Vinf_p);
F2 = powered_ga(x0(2),turn_angle,muU,Vinf_m,Vinf_p);

if F1*F2 > 0
    Delta_vel_hyp = inf;
else
    [r_peri,~,~] = fzero(@(x)powered_ga(x,turn_angle,muU,Vinf_m,Vinf_p),...
        x0);
end

E1 = 1 + r_peri*(Vinf_m^2)/muU;   % eccentricity
E2 = 1 + r_peri*(Vinf_p^2)/muU;

turnangle1 = 2 * asin( 1/E1 );    % turn angle
turnangle2 = 2 * asin( 1/E2 );

h1 = muU / Vinf_m * sqrt(E1^2 - 1); % momentum vector
h2 = muU / Vinf_p * sqrt(E2^2 - 1);

V_peri1 = h1 / r_peri;
V_peri2 = h2 / r_peri;
    
Delta_vel_hyp = V_peri2 - V_peri1;

%  - 3D Plot of hyperbolas around Uranus
[i_h,OM_h,om_h] = hyp3D(Vinf_minus,Vinf_plus,E2);

%%%%%%%% Arc of "arrival" hyperbola
r_peri; E1; turnangle1; Vinf_m;
a_1 = -muU/(Vinf_m)^2;                             % Semi major axis of the incoming hyperbola
th_inf1 = deg2rad(120);                            % Imposed angle
dt1 = theta2time(a_1,E1,muU,0,th_inf1);            % Time from th_inf to pericenter 
par_hyp1 = [a_1;E1;i_h;OM_h;om_h;-th_inf1];         % Parameters of hyperbola
[r1_vec,v1_vec] = kep2car(par_hyp1(1),par_hyp1(2),par_hyp1(3),...
    par_hyp1(4),par_hyp1(5),par_hyp1(6),muU);

% - Time interval for integration of orbitaldynamics
tspan1 = [0 dt1];

% - Integration and plot along the first branch of hyperbola
options = odeset('Reltol',1e-13, 'AbsTol',1e-14);
[T_hyp1, X_hyp1] = ode113(@orbitaldynamics,tspan1,[r1_vec, v1_vec],options,muU);
Nfigure = Nfigure +1;
figure(Nfigure)
grid on 
plot3(X_hyp1(:,1),X_hyp1(:,2),X_hyp1(:,3),'red','LineWidth',3)
hold on

%%%%%%%% Arc of "departure" hyperbola
r_peri; E2; turnangle2; Vinf_p;
a_2 = -muU/(Vinf_p)^2;                             % Semi major axis of the outcoming hyperbola
th_inf2 = deg2rad(120);                            % Imposed angle
delta_t2 = theta2time(a_2,E2,muU,0,th_inf2);       % Time from pericenter to th_inf
par_hyp2 = [a_2;E2;i_h;OM_h;om_h;0];               % Parameters of hyperbola
[rr_hyp2,vv_hyp2] =  kep2car(par_hyp2(1),par_hyp2(2),par_hyp2(3),par_hyp2(4),par_hyp2(5),par_hyp2(6),muU);
Period_hyp2 = delta_t2;
tspan2 = [0 Period_hyp2];
X0_hyp2 = [rr_hyp2;vv_hyp2];
options = odeset('Reltol',1e-13, 'AbsTol',1e-14);
[T_hyp2, X_hyp2] = ode113(@orbitaldynamics, tspan2, X0_hyp2, options, muU);



% - Plot Hyperbola
scaleU = 5; 
figure(Nfigure)
grid on
hold on
plot3(X_hyp2(:,1),X_hyp2(:,2),X_hyp2(:,3),'blue','LineWidth',3);
quiver3(0,0,0,vv_Uranus(1),vv_Uranus(2),vv_Uranus(3),100000)
drawPlanet( 'Uranus',ID_U + 20,[0,0,0], scaleU)
axis equal
title('2D hyperbolas around Uranus','Interpreter','latex')
legend('Incoming hyperbola','Outcoming hyperbola',...
    'Velocity of the planet','Uranus')
xlabel('X axis [km]','Interpreter','latex')
ylabel('Y axis [km]','Interpreter','latex')
zlabel('Z axis [km]','Interpreter','latex')

hold off

toc
