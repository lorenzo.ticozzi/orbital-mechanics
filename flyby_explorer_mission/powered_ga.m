function P = powered_ga(x,delta,mu_P,v1,v2)
%--------------------------------------------------------------------------
%
% PROTOTYPE :  P = powered_ga(x,delta,mu_P,v1,v2)
%
% DESCRIPTION: Function useful to find the radius of perigee of the 
%              hyperbola using the bisection algorithm. 
%
% INPUT:    delta       turn angle computed starting from the the two
%                       vectorial incoming and outcoming velocities 
%                       
%           mu_P        gravitational parameter of the planet around which
%                       the flyby is performed
%
%           v1          Incoming velocity of the S/C  (norm)      
%
%           v2          Outcoming "            "          "
%
% OUTPUT:   P           Function describing the pericenter radius
%
% FUNCTIONS CALLED:   -------
%
% AUTHOR:             Michele Maranesi, Lorenzo Ticozzi and Salvador Ribes, 
%                     25/07/2018, MATLAB
%
% CHANGELOG:          ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------
% the unknown x is the pericenter radius

P = asin(1./(1 + x*(v1^2)./mu_P)) + asin(1./(1 + x*(v2^2)./mu_P)) - delta;


return