%--------------------------------------------------------------------------
function [Delta_vel] = GravityAssist (muP,Vinf_minus,Vinf_plus,R_MIN,R)
%--------------------------------------------------------------------------
%
%    PROTOTYPE: 
% [Delta_vel] = GravityAssist (muP,Vinf_minus,Vinf_plus,R_MIN,R)
%
%    DESCRIPTION: 
% This function analyses the gravity assist, finding all the parameters to
% define the correct hyperbola to compute the powered gravity assist 
% calculating mainly the delta velocity to give at the pericenter of the
% hyperbola.
%
%    INPUT
% muP           = Standard gravitational parameter of the planet [km^3/s^2]
% Vinf_minus    = Vector of velocity @ infinite - [km/s]
% Vinf_plus     = Vector of velocity @ infinite + [km/s]
% R_MIN         = Minimum possible pericenter radius for hyperbola [km] 
%
%    OUTPUT
% Delta_vel    = DV required at the pericenter to perform the manoeuvre
%
%    NOTE 
% The semi-major axis, positions, times, and gravitational parameter
% must be in compatible units.
%
%    CALLED FUNCTIONS:
% influence.m, fzero.m, powered_ga.m
%
%    FUTURE DEVELOPMENT:
% ---
%
%    AUTHOR:
% Michele Maranesi and Lorenzo Ticozzi, 23/07/2018, MATLAB, 
% GravityAssist.m
%
%    CHANGELOG:
% 26/07/2018, Michele Maranesi: added more order
% 14/08/2018, Lorenzo Ticozzi:  correction of the definition of the turn
%                               angle in line 51
% 30/08/2018, Michele Maranesi Lorenzo Ticozzi and Ribes Salvador: cor-
%                               rection of F1 and F2 from line 65 to 68
%                          
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

Vinf_m = norm(Vinf_minus);
Vinf_p = norm(Vinf_plus);

turn_angle = acos((dot(Vinf_minus,Vinf_plus))/(Vinf_m*Vinf_p));

    

% - Solution of nonlinear equation to find the pericenter radius
r_SOI = influence(muP,R);
x0 = [R_MIN r_SOI];

F1 = powered_ga(x0(1),turn_angle,muP,Vinf_m,Vinf_p);
F2 = powered_ga(x0(2),turn_angle,muP,Vinf_m,Vinf_p);

if F1*F2 > 0
    Delta_vel = inf;
else
[r_peri,~,~] = fzero(@(x)powered_ga(x,turn_angle,muP,Vinf_m,Vinf_p),x0);

E1 = 1 + r_peri*(Vinf_m^2)/muP;
E2 = 1 + r_peri*(Vinf_p^2)/muP;
    
turn1 = 2 * asin( 1/E1 );
turn2 = 2 * asin( 1/E2 );
    
    
%Now we have to calculate the Delta_vel @ pericenter of 2 hyperbolas 
%v_peri= h / r_peri
    
h1 = muP / Vinf_m * sqrt(E1^2 - 1);
h2 = muP / Vinf_p * sqrt(E2^2 - 1);
  
V_peri1 = h1 / r_peri;
V_peri2 = h2 / r_peri;
    
Delta_vel = V_peri2 - V_peri1;

end
    
return
    
    




