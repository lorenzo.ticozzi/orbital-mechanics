
% -------------------------------------------------------------------------
function [dep_date,flyby_date,arr_date,best_TOF1,best_TOF2,minDV] = triple4(early_dep,late_dep,early_flyby,late_flyby,early_arr,late_arr)
%--------------------------------------------------------------------------
%
% PROTOTYPE :       [dep_date,flyby_date,arr_date,best_TOF1,best_TOF2,minDV] = triple4(early_dep,late_dep,early_flyby,late_flyby,early_arr,late_arr)
%
% DESCRIPTION:      Research of the best Flyby Explorer Mission cost with 
%                   the analysis of Porkchops, that is using the "triple 
%                   for strategy" but starting with dates analyzed with the
%                   Genetic Algorithm
%
% INPUT:            early_dep      Vector of early departure date [1x6] 
%                   late_dep       Vector of late departure date [1x6]
%                   early_flyby    Vector of early fly-by date [1x6]
%                   late_flyby     Vector of late fly-by date [1x6]
%                   early_arr      Vector of early arrival date [1x6]
%                   late_arr       Vector of late arrival date [1x6]
%
% OUTPUT:           dep_date       Vector of exact departure date to have 
%                                  the minimum mission cost [1x6]
%                   flyby_date     Vector of exact fly-by date to have 
%                                  the minimum mission cost [1x6]
%                   arr_date       Vector of exact arrival date to have 
%                                  the minimum mission cost [1x6]
%                   best_TOF1      Days to go from Neptune to Uranus
%                   best_TOF2      Days to go from Uranus to Jupiter
%                   minDV          Cost mission [km/s]
%
% FUNCTIONS CALLED: date2mjd2000.m
%                   mjd20002date.m
%                   uplanet.m
%                   kep2car.m
%                   lambertMR.m
%                   GravityAssist.m
%
% AUTHOR:           Michele Maranesi, Lorenzo Ticozzi and Salvador Ribes, 
%                   01/08/2018, MATLAB
%
% CHANGELOG:        ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

% - Definitions of global variables 
global orbitType Nrev Ncase optionsLMR muU muS ID_N ID_U ID_J hatm radU...
    tstep_N tstep_U tstep_J RU

% - Definitions of mjd2000 days
early_dep = date2mjd2000(early_dep);
late_dep = date2mjd2000(late_dep);

early_flyby = date2mjd2000(early_flyby);
late_flyby = date2mjd2000(late_flyby);

early_arr = date2mjd2000(early_arr);
late_arr = date2mjd2000(late_arr);

t_dep_vec = early_dep : tstep_N : late_dep;            % Possible dep days
t_flyby_vec = early_flyby : tstep_U : late_flyby;      % Possible flyby days
t_arr_vec = early_arr : tstep_J : late_arr;            % Possible arr days

n = length(t_dep_vec);
m = length(t_flyby_vec);
l = length(t_arr_vec);

% Initialize vectors of dates
dep_dates = zeros(length(t_dep_vec),6);
flyby_dates = zeros(length(t_flyby_vec),6);
arr_dates = zeros(length(t_arr_vec),6);

% - Initialize matrix of DV_tot
DV_tot_matrix = zeros(n,m,l);

h = waitbar(0,'Please wait...');

for i = 1 : n
    dep_day = t_dep_vec(i);                     % Julian day @departure
    dep_dates(i,:) = mjd20002date(dep_day);     % Departure dates
    [kep_Nept, muS] = uplanet(dep_day,ID_N);   % Kep par of Neptune @departure
    
    % Keplerian parameters for Neptune @ departure date
    a_N = kep_Nept(1);
    e_N = kep_Nept(2);
    i_N = kep_Nept(3);
    OM_N = kep_Nept(4);
    om_N = kep_Nept(5);
    theta_N = kep_Nept(6);
    
    [rN_dep, vN_dep] = kep2car(a_N,e_N,i_N,OM_N,om_N,theta_N, muS);
    waitbar(i/n)
    
    for j = 1 : m
        flyby_day = t_flyby_vec(j);                   % Julian day @flyby
        flyby_dates(j,:) = mjd20002date(flyby_day);   % Flyby dates
        [kep_Uran, muS] = uplanet(flyby_day,ID_U);   % Kep par of Uranus @flyby
        TOF1_days = flyby_day - dep_day;              % TOF Neptune -> Uranus
        TOF1 = TOF1_days * 86400;
        
        % Keplerian parameters for Uranus @ flyby date
        a_U = kep_Uran(1);
        e_U = kep_Uran(2);
        i_U = kep_Uran(3);
        OM_U = kep_Uran(4);
        om_U = kep_Uran(5);
        theta_U = kep_Uran(6);
    
        [rU_flyby, vU_flyby] = kep2car(a_U,e_U,i_U,OM_U,om_U,theta_U, muS);
        
        % First Lambert arc - Neptune -> Uranus
        [~,~,~,~,VI1,VF1,~,~] = lambertMR(rN_dep,rU_flyby,TOF1,muS,orbitType,Nrev,Ncase,optionsLMR);
        
        % DVs for the first arc
        DV_dep = norm(VI1 - vN_dep);
        Vinf_minus = VF1 - vU_flyby;
        
        for k = 1 : l
            arr_day = t_arr_vec(k);
            arr_dates(k,:) = mjd20002date(arr_day);
            [kep_Jup, muS] = uplanet(arr_day,ID_J);   % Kep par of Jupiter @flyby
            TOF2_days = arr_day - flyby_day;           % TOF Uranus -> Jupiter
            TOF2 = TOF2_days * 86400;
            
            % Keplerian parameters for Jupiter @ arrival date
            a_J = kep_Jup(1);
            e_J = kep_Jup(2);
            i_J = kep_Jup(3);
            OM_J = kep_Jup(4);
            om_J = kep_Jup(5);
            theta_J = kep_Jup(6);
            
            [rJ_arr, vJ_arr] = kep2car(a_J,e_J,i_J,OM_J,om_J,theta_J, muS);
            
            % Second Lambert arc - Uranus -> Jupiter
            [~,~,~,~,VI2,VF2,~,~] = lambertMR(rU_flyby, rJ_arr,...
                TOF2,muS,orbitType,Nrev,Ncase,optionsLMR);
            Vinf_plus = VI2 - vU_flyby;
            
            % DVs for the second arc - the first DV of the Lambert arc is
            % given using a powered gravity assist around Uranus
            R_MIN = radU + hatm;
            [Delta_vel] = GravityAssist (muU,Vinf_minus,Vinf_plus,R_MIN,RU);
            DV_flyby = norm(Delta_vel);
            
            DV_arr = norm(vJ_arr - VF2);
            
            % Total DV for the manoeuvre
            DV_tot = DV_dep + DV_flyby + DV_arr;
            
            DV_tot_matrix(i,j,k) = DV_tot;
            
        end
    end
end

% - Find the position in the matrix of the minimum DV value
minDV = min(min(min(DV_tot_matrix)));
[row,col,matrix] = ind2sub(size(DV_tot_matrix),find(DV_tot_matrix == minDV));

% - Significant dates for the transfer
dep_date = dep_dates(row,:);
flyby_date = flyby_dates(col,:);
arr_date = arr_dates(matrix,:);

% - Optimal TOFs
best_TOF1 = date2mjd2000(flyby_date) - date2mjd2000(dep_date);
best_TOF2 = date2mjd2000(arr_date) - date2mjd2000(flyby_date);

return
