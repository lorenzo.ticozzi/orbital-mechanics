function [Xd] = orbitaldynamics(t,X,mu)

%--------------------------------------------------------------------------
%
% PROTOTYPE:        [Xd] = orbitaldynamics(t,X,mu)
%
% DESCRIPTION:      The following function describes the 3D dynamics of a
%                   two-body problem.
%
% INPUT: t          Fake variable (no explicit time dependence in the
%                   equation).
%        X          State vector which must be initialized as a (6x1)
%                   vector.
%        mu         Gravitational constant for the main attractor of the
%                   orbital problem under analysis.
%
% OUTPUT: Xd        Output of the function is the derivative of the state
%                   vector.
%
% FUNCTIONS CALLED: ---------
%
% AUTHOR:            Michele Maranesi, Lorenzo Maria Ticozzi and Salvedor 
%                    Ribes, 23/07/2018, MATLAB
%   
% CHANGELOG:         ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

% Definition of the state vector X

rx = X(1);   % r are the components of the position vector
ry = X(2);
rz = X(3);
vx = X(4);   % v are the components of the velocity vector
vy = X(5);
vz = X(6);

rx_d = vx; 
ry_d = vy;
rz_d = vz;

% The second order differential problem is rewritten as a first order
% system

vx_d = -mu / ((rx^2 + ry^2 + rz^2)^(3/2))*rx;  
vy_d = -mu / ((rx^2 + ry^2 + rz^2)^(3/2))*ry;
vz_d = -mu / ((rx^2 + ry^2 + rz^2)^(3/2))*rz;

Xd = [rx_d ry_d rz_d vx_d vy_d vz_d]';

return







