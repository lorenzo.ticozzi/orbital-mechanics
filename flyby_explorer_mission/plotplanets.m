
% -------------------------------------------------------------------------
function h = plotplanets(ID_P,date1,sun,Color,scale,Nfigure,date2)
%--------------------------------------------------------------------------
%
% PROTOTYPE :   h = plotplanets(ID_P,date1,sun,Color,scale,Nfigure,date2)
%
% DESCRIPTION:  The aim of this function is to plot the orbit.
%
% INPUT :       ID_P  - number with the name of the planet
%               date1 - [yyyy mm dd HH MM SS] vector expressing the date in
%                       which the orbit is beginning
%               sun   - useful for the presence of the in the origin of
%                       axis
%                       sun == 1  yes Sun
%                       sun == 0  no Sun
%               Color - string expressing the color of the orbit 
%               scale - scale of the planet
%               Nfigure - scalar expressing number of the figure
%               date2 - [yyyy mm dd HH MM SS] vector expressing the date in
%                       which the orbit is ending
%               
% OUTPUT:       ------
%
% FUNCTIONS CALLED: date2mjd2000.m
%                   uplanet.m
%                   kep2car.m
%                   drawPlanet.m
%                   ode113.m
%                   orbitaldynamics.m
%
% AUTHOR:       Michele Maranesi, Lorenzo Ticozzi and Salvador Ribes, 
%               31/08/2018, MATLAB
%
% CHANGELOG:    ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

global muS

switch ID_P
    case 1
        planet = 'Mercury';
    case 2
        planet = 'Venus';
    case 3
        planet = 'Earth';
    case 4
        planet = 'Mars';
    case 5
        planet = 'Jupiter';
    case 6
        planet = 'Saturn';
    case 7
        planet = 'Uranus';
    case 8
        planet = 'Neptune';
    case 9
        planet = 'Pluto';
    otherwise
        error('Wrong ID for departure planet')
end

% - String containing the name of the planet
PlanetName = sprintf('%s',planet);

% - Change dates to mjd2000
date1 = date2mjd2000(date1);

[kepP, muS] = uplanet(date1,ID_P);
[rP1, vP1] = kep2car(kepP(1),kepP(2),kepP(3),kepP(4),kepP(5),...
    kepP(6),muS);

if nargin < 7
    
    % - Plot only the planet in its position
    h = figure(Nfigure);
    drawPlanet(PlanetName,ID_P+20,rP1,scale);
    period = 2*pi*sqrt((kepP(1))^3 / muS);
    hold on
    
    % - Plot the whole orbit of the planet
    options = odeset('Reltol',1e-13,'Abstol',1e-14);
    X0 = [rP1', vP1'];
    tspan = [0 period];
    [~,X] = ode113(@orbitaldynamics,tspan,X0,options,muS);
    rP_x = X(:,1);
    rP_y = X(:,2);
    rP_z = X(:,3);

    % - Plot the whole orbit
    plot3(rP_x,rP_y,rP_z,'black')
    axis equal
    
else
    
    % - Solve the orbital dynamics for the desired interval
    options = odeset('Reltol',1e-13,'Abstol',1e-14);
    X0 = [rP1', vP1'];
    date2 = date2mjd2000(date2);
    tspan_days = date2 - date1;
    tspan_sec = tspan_days * 86400;
    tspan = [0 tspan_sec];
    [~,X] = ode113(@orbitaldynamics,tspan,X0,options,muS);
    rP_x = X(:,1);
    rP_y = X(:,2);
    rP_z = X(:,3);
    
    % - Highlight the desired part of orbit
%     Nfigure = Nfigure + 1;
    h = figure(Nfigure);
    plot3(rP_x,rP_y,rP_z,Color,'LineWidth',1.5)
    axis equal
    hold on
    
    % - Plot the whole orbit
    period = 2*pi * sqrt((kepP(1))^3 / muS);
    tspan_tot = [0 period];
    [~,Xtot] = ode113(@orbitaldynamics,tspan_tot,X0,options,muS);
    rP_xtot = Xtot(:,1);
    rP_ytot = Xtot(:,2);
    rP_ztot = Xtot(:,3);
    
    plot3(rP_xtot, rP_ytot, rP_ztot,'black');
    
    % - Plot the planet
    planet_pos = [rP_x(1),rP_y(1),rP_z(1)];
    drawPlanet(PlanetName,ID_P+20,planet_pos,scale);
    hold on
end

% - Plot of the Sun (if desired)
if sun == 1
    drawPlanet('Sun',3,[0,0,0],5e+2);
else
    return
end











