
% -------------------------------------------------------------------------
function [i,OM,om] = hyp3D(Vinf_minus,Vinf_plus,E2)
% -------------------------------------------------------------------------

% hyp3D.m 
%
%  PROTOTYPE:    [i,OM,om] = hyp3D(Vinf_minus,Vinf_plus,E2)
%  
%
%  DESCRIPTION: This function computes the inclination, the right ascension
%               of the ascending node and the anomaly of the pericenter of 
%               an hyperbolic orbit of which we know the two velocities at 
%               the entrance of the sphere of influence, and the 
%               eccentricity of the second branch of the hyperbola, as the
%               two branches have different eccentricities due to the DV at
%               the pericenter.
%               
%
%  INPUT :      Vinf_minus - (3x1) column vector describing the velocity of
%                            the S/C at the entrance of the sphere of
%                            influence of the planet
%               Vinf_plus  - (3x1) column vector describing the velocity of
%                            the S/C at the entrance of the sphere of
%                            influence of the planet
%               E2         - Eccentricity of the outcoming hyperbola branch
%
%  OUTPUT:      i    -    Inclination of the orbit
%               OM   -    Right ascension of the ascending node
%	            om   -    Anomaly of the pericenter
%
%  FUNCTIONS CALLED:      
%
%  AUTHORS:                 Lorenzo Maria Ticozzi
%                           Michele Maranesi
%                           Salvador Ribes Izquierdo
%
% -------------------------------------------------------------------------

% - Definition of the versors of the cartesian frame
X = [1 0 0]';
Y = [0 1 0]';
Z = [0 0 1]';


% - Find the inclination exploiting the two known velocities
K_vec = cross(Vinf_minus,Vinf_plus);
K_norm = norm(K_vec);
i = acos(dot(K_vec,Z)/K_norm);

% - Find the node vector
N_vec = cross(Z,K_vec);
N_norm = norm(N_vec);

if dot(Y,N_vec) >= 0
    OM = acos(dot(X,N_vec)/N_norm);
else
    OM = 2*pi - acos(dot(X,N_vec)/N_norm);
end

% - Find the pericenter anomaly by rotating the Vinf_plus vector of an
% angle equal to theta_inf
theta_inf = acos(-1/E2);
ROT = [ cos(-theta_inf) sin(-theta_inf)  0;
        -sin(-theta_inf) cos(-theta_inf)  0;
               0           0          1];
E_vec = ROT' * Vinf_plus;
E_norm = norm(E_vec);

if dot(Z,E_vec) >= 0
    om = acos(dot(N_vec,E_vec)/(E_norm*N_norm));
else
    om = 2*pi - acos(dot(N_vec,E_vec)/(E_norm*N_norm));
end

om=0;

return






