function delta_theta = time2theta(a,e,mu,t_i,t_f)

% time2theta.m computes the theta angle (true anomaly) corresponding to a
% certain period of time.
%
% PROTOTYPE: theta = time2theta(a,e,mu,t_i,t_f,h)
%
% DESCRIPTION: the time2theta.m implements the computation of the angle
%              covered by a body on a certain orbit in a certain timelapse.
%              The t_i, t_f values are calculated starting from a zero time
%              in the perigee (theta = 0). 
%
% INPUT:  a            Semi-major axis of the orbit
%         e            Eccentricity of the orbit
%         h            Angular momentum of the orbit
%         mu           Gravitational constant of the main attractor
%         t_i [s]      Initial time instant
%         t_f [s]      Final time instant
%
% OUTPUT: theta [rad]  Angle corresponding to the given interval of time
%
%--------------------------------------------------------------------------

if nargin < 5
    error('Not enough input arguments')
end


% Options for 'fzero'
options = optimset('fzero');
options.Display = 'notify';
options.TolX = 1e-5;

% Parameter p of the orbit
p = a*(1-e^2);

% Solution of the problem for different types of orbits
%
% 1. Elliptical orbit (0<e<1)

if (e >= 0) && (e<1)
    E_i = fzero(@(x)elliptic_time(x,t_i,a,e,mu),0,options); % Solution to find E
    E_f = fzero(@(x)elliptic_time(x,t_f,a,e,mu),0,options);
    
    theta_i = 2*atan(sqrt((1+e)/(1-e))*tan(E_i/2));  % True anomalies
    if theta_i < 0                                  % check on the sign of
        theta_i = theta_i + 2*pi;                    % theta
    end
    
    theta_f = 2*atan(sqrt((1+e)/(1-e))*tan(E_f/2));
    if theta_f <= 0                                  
        theta_f = theta_f + 2*pi;                   
    end
    
    delta_theta = theta_f - theta_i;                 % Angular distance
    
% 2. Hyperbolic orbit

elseif e > 1
    F_i = fzero(@(x)hyperbolic_time(x,t_i,a,e,mu),0,options);
    F_f = fzero(@(x)hyperbolic_time(x,t_f,a,e,mu),0,options);
    
    theta_i = 2*atan(sqrt((e+1)/(e-1))*tanh(F_i/2));
    if theta_i <= 0
        theta_i = theta_i + 2*pi;
    end
    
    theta_f = 2*atan(sqrt((e+1)/(e-1))*tanh(F_f/2));
    if theta_f <= 0
        theta_f = theta_f + 2*pi;
    end
    
    delta_theta = theta_f - theta_i;
    
% 3. Parabolic orbit

elseif e == 1
    D_i = fzero(@(x)parabolic_time(x,t_i,a,e,mu),0,options);
    D_f = fzero(@(x)parabolic_time(x,t_f,a,e,mu),0,options);
    
    theta_i = 2*atan(D_i);
    if theta_i <= 0
        theta_i = theta_i + 2*pi;
    end
    
    theta_f = 2*atan(D_f);
    if theta_f <= 0
        theta_f = theta_f + 2*pi;
    end
    
    delta_theta = theta_f - theta_i;
    
else 
    error('Error: check value of e')
end


return








