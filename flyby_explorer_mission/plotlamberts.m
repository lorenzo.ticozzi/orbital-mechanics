
% -------------------------------------------------------------------------
function h = plotlamberts(r,v,date1,date2,Color,Nfigure)
%--------------------------------------------------------------------------
%
% PROTOTYPE :   h = plotlamberts(r,v,date1,date2,Color,Nfigure)
%
% DESCRIPTION:  The aim of this function is to plot the transfer arc 
%               descripted by the Lambert's problem.
%
% INPUT :       r  -  (3x1) vector position of the satellite at the 
%                     beginning of the Lambert arc
%               v  -  (3x1) velocity vector of the satellite at the 
%                     beginning of the Lambert arc
%               date1 - [yyyy mm dd HH MM SS] vector expressing the date in
%                       which the transfer is beginning
%               date2 - [yyyy mm dd HH MM SS] vector expressing the date in
%                       which the transfer is ending
%               Color - string expressing the color of the Lambert arc
%               Nfigure - scalar expressing number of the figure
%               
% OUTPUT:       ------
%
% FUNCTIONS CALLED: date2mjd2000.m
%                   ode113.m
%                   orbitaldynamics.m
%
% AUTHOR:       Michele Maranesi, Lorenzo Ticozzi and Salvador Ribes, 
%               31/08/2018, MATLAB
%
% CHANGELOG:    ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------

global muS

X0 = [r',v'];
tspan_days = date2mjd2000(date2) - date2mjd2000(date1);
tspan_sec = tspan_days * 86400;
tspan = [0 tspan_sec];

% - Solve the orbital dynamics
options = odeset('Reltol',1e-13,'Abstol',1e-14);
[~,X] = ode113(@orbitaldynamics,tspan,X0,options,muS);
rx = X(:,1);
ry = X(:,2);
rz = X(:,3);

% - Plot the desired part of the orbit
h = figure(Nfigure);
plot3(rx,ry,rz,Color,'LineWidth',1.5)

return








