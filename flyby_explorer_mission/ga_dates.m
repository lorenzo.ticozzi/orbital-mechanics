
%--------------------------------------------------------------------------
function [opt_dates, fval, exitflag] = ga_dates(LB,UB)
%--------------------------------------------------------------------------
%
%  PROTOTYPE:    [opt_dates, fval, ~] = ga_dates(LB,UB,options);
%  
%  DESCRIPTION:  the following function uses the 'ga' function from Matlab
%                to solve the optimization problem linked to the
%                interplanetary transfer.
%               
%  INPUT :       LB  Column vector containing the values of departure date, 
%                    TOF1 and TOF2 constituting the lower bound for the 
%                    transfer strategy. These data are in MJD2000 values 
%                    LB = [early_dep; shortest_TOF1; shortest_TOF2]
%                UB  Column vector containing the values of departure date, 
%                    TOF1 and TOF2 constituting the upper bound for the 
%                    transfer strategy. These data are in MJD2000 values 
%                    UB = [late_dep; longest_TOF1; longest_TOF2]
%
%  OUTPUT :      --------
%
%  FUNCTIONS CALLED:       ga.m
%                          DV.m
%
%  AUTHOR:                 Lorenzo Maria Ticozzi, 15/8/2018, MATLAB
%   
%  CHANGELOG:              Michele Maranesi and Lorenzo Maria Ticozzi, 
%                          30/08/2018: added options about PopulationSize
%                                      in line 41.
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------


% - Options for 'ga' function
options = optimoptions('ga','PlotFcn', {@gaplotbestf, @gaplotbestindiv});
options.MaxGenerations = 1000;
options.PopulationSize = 200;

% - Optimization of the dates
[optima, fval, exitflag] = ga(@DV,3,[],[],[],[],LB,UB,[],options);

opt_dep_day = optima(1);
opt_TOF1 = optima(2);
opt_TOF2 = optima(3);

opt_dates = [opt_dep_day opt_TOF1 opt_TOF2];

return









