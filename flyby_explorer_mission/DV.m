
%--------------------------------------------------------------------------
function [DV_tot] = DV(time)
%--------------------------------------------------------------------------
%
%  PROTOTYPE:    [DV_tot] = DV(time)
%
%  DESCRIPTION: 
% The following function computes the total DV due to the overall transfer
% from Neptune to Jupiter, with a flyby around Uranus. It is the function
% used to run the genetic algorithm ('ga' function of the Optimization 
% Toolbox) to optimize the interplanetary strategy in terms of DV. 
%               
%  INPUT :      time       (1x3) row vector describing the two different
%                          TOFs, from Neptune to Uranus and from Uranus to
%                          Jupiter, and the date (mjd2000) of the initial 
%                          departure from Neptune in the gregorian
%                          calendar.
%
%	                       time = [dep_date TOF1 TOF2]
%
%                          N.B: all the times in the input have to be in
%                          days (both departure and TOFs)
%
%  OUTPUT:      DV_tot     total DV of the transfer
%	
%  FUNCTIONS CALLED:       uplanet.m
%                          kep2car.m
%                          lambertMR.m
%                          astroConstants.m
%                          GravityAssist.m
%
%  FUTURE DEVELOPMENT:     ---------
% 
%  AUTHOR:                 Lorenzo Maria Ticozzi, 25/7/2018, MATLAB
%   
%  CHANGELOG:              ---------
%   
% Note: Please if you have got any changes that you would like to be done,
% do not change the function, please contact the author.
%
%--------------------------------------------------------------------------


% - Definition of global variables
global orbitType Nrev Ncase optionsLMR muS muU ID_N ID_U ID_J radU hatm RU


%%% --- Velocity and position of Neptune at the departure day --- %%%
dep_date = time(1);

[kepN,muS] = uplanet(dep_date, ID_N);
a_N = kepN(1);
e_N = kepN(2);
i_N = kepN(3);
OM_N = kepN(4);
om_N = kepN(5);
th_N = kepN(6);

[rN, vN] = kep2car(a_N,e_N,i_N,OM_N,om_N,th_N,muS);

% Velocity and position of Uranus at the flyby day
TOF1 = time(2)*86400;                   % TOF from Neptune to Uranus [s]
flyby_date = dep_date + time(2);        % Date of flyby (mjd2000)

[kepU,muS] = uplanet(flyby_date, ID_U);
a_U = kepU(1);
e_U = kepU(2);
i_U = kepU(3);
OM_U = kepU(4);
om_U = kepU(5);
th_U = kepU(6);

[rU, vU] = kep2car(a_U,e_U,i_U,OM_U,om_U,th_U,muS);

% Lambert arc from Neptune to Uranus with the specified TOF1 and dep date
[~,~,~,~,VI1,VF1,~,~] = lambertMR(rN,rU,TOF1,muS,orbitType,Nrev,Ncase,...
    optionsLMR);


% DV1 -> Neptune to first Lambert arc
DV1 = VI1 - vN;            % This DV is given by the rocket engine
DV1_norm = norm(DV1);      % Norm of the first DV


% Velocity and position of Jupiter at the arrival day
TOF2 = time(3)*86400;               % TOF from Uranus to Jupiter
arr_date = flyby_date + time(3);    % Date of arrival to Jupiter (mjd2000)

[kepJ,muS] = uplanet(arr_date, ID_J);
a_J = kepJ(1);
e_J = kepJ(2);
i_J = kepJ(3);
OM_J = kepJ(4);
om_J = kepJ(5);
th_J = kepJ(6);

[rJ, vJ] = kep2car(a_J,e_J,i_J,OM_J,om_J,th_J,muS);

% Lambert arc from Uranus to Jupiter with the specified TOF2
[~,~,~,~,VI2,VF2,~,~] = lambertMR(rU,rJ,TOF2,muS,orbitType,Nrev,Ncase,...
    optionsLMR);

% DV2 -> Second Lambert arc to Jupiter
DV2 = vJ - VF2;           % DV to insert the S/C around Jupiter
DV2_norm = norm(DV2);


% Now the velocities needed for the flyby solution are computed: they are
% the difference between the incoming (or outcoming) velocity of the S/C
% and the velocity of Uranus in helio ref sys

V_inf1 = VF1 - vU;
V_inf2 = VI2 - vU;

% Solution of the gravity assist around Uranus -> DV for the manoeuvre
R_MIN = radU + hatm;
[DV3] = GravityAssist (muU,V_inf1,V_inf2,R_MIN,RU);

DV3_norm = abs(DV3);           % Norm of the DV at the pericenter of the hyperbola

DV_tot = DV1_norm + DV2_norm + DV3_norm;      % norm [km/s]

return











